/**
 * Learn OpenGL Part 02 - Hello Triangle
 *
 * $ gcc -o bin/${1} ${1}/main.c -lglfw -lGL -lX11 -lpthread -lXrandr -lXi -ldl \
 * > -Ilibs/glad/include libs/glad/src/glad.c
 */

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <unistd.h>

const int WIDTH = 680;
const int HEIGHT = 480;

const char *vertexShaderSource =
    "#version 150 core\n"
    "in vec2 position;\n"
    "in vec3 color;\n"
    "out vec3 Color;\n"
    "void main() {\n"
    "   Color = color;"
    "   gl_Position = vec4(position, 0.0, 1.0);\n"
    "}\n\0";

const char *fragmentShaderSource =
    "#version 150 core\n"
    "in vec3 Color;\n"
    "out vec4 outColor;\n"
    "void main() {\n"
    "   outColor = vec4(Color, 1.0);\n"
    "}\n\0";

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void handleInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, 1);
}

int main(int argc, char const *argv[])
{
    if (!glfwInit())
    {
        fprintf(stderr, "glfw: Initialization failed\n");
        return 1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", NULL, NULL);
    if (!window)
    {
        fprintf(stderr, "glfw: Failed to create window\n");
        glfwTerminate();
        return 1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        fprintf(stderr, "glad: Failed to initialize glad\n");
        glfwTerminate();
        return 1;
    }

    // VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // glViewport(0, 0, WIDTH, HEIGHT);

    GLuint vbo;
    glGenBuffers(1, &vbo);

    // clang-format off
    GLfloat vertices[] = {
         0.0f,  0.5f, 1.0f, 0.0f, 0.0f,  // Vertex 1 (X, Y)
         0.5f, -0.5f, 0.0f, 1.0f, 0.0f,  // Vertex 2 (X, Y)
        -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,  // Vertex 3 (X, Y)
    };
    // clang-format on

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // compile vertex shader
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    GLint status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        fprintf(stderr, "vertex shader failed to compile\n");
    }

    // compite fragment shader
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        fprintf(stderr, "fragment shader failed to compile\n");
    }

    // Combining shaders into a program
    GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glBindFragDataLocation(shaderProgram, 0, "outColor");
    // Linking Program
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);

    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE,
                          5 * sizeof(GLfloat), 0);

    GLint colAttrib = glGetAttribLocation(shaderProgram, "color");
    glEnableVertexAttribArray(colAttrib);
    glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE,
                          5 * sizeof(GLfloat), (void *)(2 * sizeof(GLfloat)));

    while (!glfwWindowShouldClose(window))
    {
        // input handling
        handleInput(window);

        // rendering
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteProgram(shaderProgram);
    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);

    glDeleteBuffers(1, &vbo);

    glDeleteVertexArrays(1, &vao);

    glfwTerminate();
    return 0;
}
