#!/bin/sh

COMPILER=gcc
LDFLAGS="-lglfw -lGL -lX11 -lpthread -lXrandr -lXi -ldl"
GLADFLAG="-Ilibs/glad/include libs/glad/src/glad.c"

${COMPILER} -o bin/${1} ${1}/main.c ${LDFLAGS} ${GLADFLAG}